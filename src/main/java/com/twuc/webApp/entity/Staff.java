package com.twuc.webApp.entity;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

@Entity
public class Staff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    //,cascade = CascadeType.ALL 不要加级联 ，
    //因为删除staff office也会被删除
    @ManyToOne(fetch = FetchType.LAZY,cascade ={CascadeType.PERSIST,CascadeType.MERGE})
    private Office office;

    public Staff() {
    }

    public Staff(String name, Office office) {
        this.name = name;
        this.office = office;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Office getOffice() {
        return office;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOffice(Office office) {
        this.office = office;
    }
}
