package com.twuc.webApp;

import com.twuc.webApp.entity.Office;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.OfficeRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class WebAppApplicationTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    //@ManyToOne 控制权在Many

    @Test
    void many_to_one() {
        Office beijing = new Office("Beijing");
        Staff staff = new Staff("manru",beijing);
//        officeRepository.save(beijing); //加上cascade ={CascadeType.PERSIST,CascadeType.MERGE}可以删除这句
        staffRepository.save(staff);
        entityManager.flush();
        entityManager.clear();
        assertEquals("Beijing",staffRepository.findAll().get(0).getOffice().getName());
    }

    //加上级联 删除staff office也会被删除
    //不加级联(CascadeType.ALL) 删除staff office不会被删除
    @Test
    void delete_staff() {
        Office beijing = new Office("Beijing");
        Staff staff = new Staff("manru",beijing);
        officeRepository.save(beijing);
        staffRepository.save(staff);

//        Staff qiqi = new Staff("77",beijing);
//        staffRepository.save(qiqi);


        entityManager.flush();
        entityManager.clear();

        staffRepository.deleteById(1L);
        entityManager.flush();
        entityManager.clear();

        assertEquals(1, officeRepository.findAll().size());
        assertEquals(0, staffRepository.findAll().size());
    }


    //解除staff1和office的关联
    @Test
    void set_waijian_null() {
        Office beijing = new Office("Beijing");
        Staff staff = new Staff("manru",beijing);
        officeRepository.save(beijing);
        staffRepository.save(staff);

        entityManager.flush();
        entityManager.clear();

        staffRepository.findAll().get(0).setOffice(null);
        entityManager.flush();
        entityManager.clear();

        assertEquals(1, officeRepository.findAll().size());
        assertEquals(1, staffRepository.findAll().size());
        assertEquals(null, staffRepository.findAll().get(0).getOffice());


        Office wuhan = new Office("Wuhan");
        staffRepository.findAll().get(0).setOffice(wuhan);
//        officeRepository.save(wuhan);

        entityManager.flush();
        entityManager.clear();

        assertEquals(2, officeRepository.findAll().size());
        assertEquals("Beijing", officeRepository.findAll().get(0).getName());
        assertEquals("Wuhan", officeRepository.findAll().get(1).getName());
        assertEquals("Wuhan", staffRepository.findAll().get(0).getOffice().getName());

    }

    //update 不用加级联也可以更新
    //office会随着staff的修改而改变
    @Test
    void update_cascade() {
        Office beijing = new Office("Beijing");
        Office wuhan = new Office("Wuhan");

        officeRepository.save(wuhan);
        officeRepository.save(beijing);
        Staff staff = new Staff("manru",beijing);
        staffRepository.save(staff);
        entityManager.flush();
        entityManager.clear();

        staffRepository.findAll().get(0).setOffice(null);
        entityManager.flush();
        entityManager.clear();
        assertEquals("Wuhan", officeRepository.findAll().get(0).getName());
        assertEquals(2, officeRepository.findAll().size());
        assertEquals("Beijing", officeRepository.findAll().get(1).getName());
        assertEquals(null, staffRepository.findAll().get(0).getOffice());
        //为什么不能把现有的office设置给staff
        staffRepository.findAll().get(0).setOffice(wuhan);
        entityManager.flush();
        entityManager.clear();
//        assertEquals("Wuhan", staffRepository.findAll().get(0).getOffice().getName());
    }

    //删不掉office,如果存在外键的引用
    @Test
    void delete_office() {
        Office beijing  = new Office("Beijing");
        Staff manru = new Staff("manru",beijing);
        officeRepository.saveAndFlush(beijing);
        staffRepository.saveAndFlush(manru);

        officeRepository.deleteById(1L);

        assertEquals(0,officeRepository.findAll().size());
    }
}